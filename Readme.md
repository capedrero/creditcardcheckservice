# Credit card check service (Provider side)

## Getting started

This is an example with a consumer check service with spring cloud contract library integration.

### Main code part
With POST endpoint `/credit-scores` wich includes:

##### CreditCardCheckScoreRequest object
---

```json
{
	"citizenNumber": "1234",
	"requestDate": "2020-03-01"
	"uuid": "445ebd04-6d71-4aa8-a5b4-854f12aeba70"
}
```
##### CreditCardCheckScoreResponse object
---
```json
{
	"score": "LOW",
	"uuid": "445ebd04-6d71-4aa8-a5b4-854f12aeba70"
}
```

### Testing part
In resources are been created **contracts** in ```resources/contracts/creditcardcheckservice/```
```groovy
package contracts.creditcardservice

import org.springframework.cloud.contract.spec.Contract

Contract.make {
    request {
        method 'POST'
        url '/credit-scores'
        body(
            "citizenNumber" : 1234,
            "requestDate" : anyDate(),
            "uuid": $(consumer(anyUuid()), producer("2bf4f912-38cd-4af2-b5e2-e4237fa32ace"))
        )
        headers {
            contentType applicationJson()
        }
    }
    response {
        status 200
        body(
            "score" : "HIGH",
            "uuid" : $(consumer(fromRequest().body('$.uuid')), producer(anyUuid()))        )
        headers {
            contentType applicationJson()
        }
    }
}
```
> As you can see, we have uuid check with "chicken or egg" paradox and we use same uuid in response stub json attibute.
We use regex wildcard with date.

### Running
We need install stubs `com.retailbank:creditcardcheckservice+`
It is using maven wrapper so in initialization:
```sh
./mvnw clean install
```