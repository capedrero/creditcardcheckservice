package com.retailbank.creditcardcheckservice.controller;

import com.retailbank.creditcardcheckservice.model.CreditCardCheckScoreRequest;
import com.retailbank.creditcardcheckservice.model.CreditCardCheckScoreResponse;
import com.retailbank.creditcardcheckservice.service.CreditCardCheckService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CreditCardCheckController {


  private CreditCardCheckService service;

  public CreditCardCheckController(
      CreditCardCheckService service) {
    this.service = service;
  }

  @PostMapping("/credit-scores")
  public CreditCardCheckScoreResponse creditScore(@RequestBody CreditCardCheckScoreRequest request){
    return service.doCreditCheck(request.getCitizenNumber(), request.getRequestDate());
  }
}
