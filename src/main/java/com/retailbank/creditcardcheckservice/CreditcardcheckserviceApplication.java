package com.retailbank.creditcardcheckservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CreditcardcheckserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CreditcardcheckserviceApplication.class, args);
	}

}
