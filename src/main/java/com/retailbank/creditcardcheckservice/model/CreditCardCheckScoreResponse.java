package com.retailbank.creditcardcheckservice.model;

import java.util.Objects;
import java.util.UUID;

public class CreditCardCheckScoreResponse {

  private Score score;
  private UUID uuid;

  public CreditCardCheckScoreResponse() {
  }

  public CreditCardCheckScoreResponse(
      Score score, UUID uuid) {
    this.score = score;
    this.uuid = uuid;
  }

  public Score getScore() {
    return score;
  }

  public void setScore(
      Score score) {
    this.score = score;
  }

  public UUID getUuid() {
    return uuid;
  }

  public void setUuid(UUID uuid) {
    this.uuid = uuid;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof CreditCardCheckScoreResponse)) {
      return false;
    }
    CreditCardCheckScoreResponse that = (CreditCardCheckScoreResponse) o;
    return score == that.score &&
        Objects.equals(uuid, that.uuid);
  }

  @Override
  public int hashCode() {
    return Objects.hash(score, uuid);
  }

  public enum Score {
    HIGH, LOW
  }

}
