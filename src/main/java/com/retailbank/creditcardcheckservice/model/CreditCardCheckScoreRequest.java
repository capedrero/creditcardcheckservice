package com.retailbank.creditcardcheckservice.model;

import java.util.Date;
import java.util.Objects;

public class CreditCardCheckScoreRequest {
  private int citizenNumber;
  private Date requestDate;

  public CreditCardCheckScoreRequest() {
  }

  public CreditCardCheckScoreRequest(int citizenNumber, Date requestDate) {
    this.citizenNumber = citizenNumber;
    this.requestDate = requestDate;
  }

  public int getCitizenNumber() {
    return citizenNumber;
  }

  public void setCitizenNumber(int citizenNumber) {
    this.citizenNumber = citizenNumber;
  }

  public Date getRequestDate() {
    return requestDate;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof CreditCardCheckScoreRequest)) {
      return false;
    }
    CreditCardCheckScoreRequest that = (CreditCardCheckScoreRequest) o;
    return citizenNumber == that.citizenNumber &&
        Objects.equals(requestDate, that.requestDate);
  }

  @Override
  public int hashCode() {
    return Objects.hash(citizenNumber, requestDate);
  }
}
