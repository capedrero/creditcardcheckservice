package com.retailbank.creditcardcheckservice;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.retailbank.creditcardcheckservice.controller.CreditCardCheckController;
import com.retailbank.creditcardcheckservice.model.CreditCardCheckScoreResponse;
import com.retailbank.creditcardcheckservice.model.CreditCardCheckScoreResponse.Score;
import com.retailbank.creditcardcheckservice.service.CreditCardCheckService;
import io.restassured.module.mockmvc.RestAssuredMockMvc;
import java.util.Date;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;

import static org.mockito.Mockito.*;


public class BaseCreditcardServiceTest {


  @BeforeEach
  public void setUp() {
    final CreditCardCheckService service = mock(CreditCardCheckService.class);
    when(service.doCreditCheck(eq(1234), any(Date.class)))
        .thenReturn(new CreditCardCheckScoreResponse(Score.HIGH, UUID
            .randomUUID()));
    when(service.doCreditCheck(eq(4444), any(Date.class)))
        .thenReturn(new CreditCardCheckScoreResponse(Score.LOW, UUID.randomUUID()));
    RestAssuredMockMvc.standaloneSetup(new CreditCardCheckController(service));

  }

}
