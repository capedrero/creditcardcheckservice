package contracts.creditcardservice

import org.springframework.cloud.contract.spec.Contract

Contract.make {
    request {
        method 'POST'
        url '/credit-scores'
        body(
            "citizenNumber" : 1234,
            "requestDate" : anyDate(),
            "uuid": $(consumer(anyUuid()), producer("2bf4f912-38cd-4af2-b5e2-e4237fa32ace"))
        )
        headers {
            contentType applicationJson()
        }
    }
    response {
        status 200
        body(
            "score" : "HIGH",
            "uuid" : $(consumer(fromRequest().body('$.uuid')), producer(anyUuid()))        )
        headers {
            contentType applicationJson()
        }
    }
}